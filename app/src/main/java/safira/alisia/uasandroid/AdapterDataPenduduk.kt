package safira.alisia.uasandroid

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_data.*
import kotlinx.android.synthetic.main.activity_main.*

class AdapterDataPenduduk(val dataPakaian : List<HashMap<String,String>>,
                         val mainActivity: MainActivity) :
    RecyclerView.Adapter<AdapterDataPenduduk.HolderDataPenduduk>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterDataPenduduk.HolderDataPenduduk {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_penduduk,parent,false)
        return HolderDataPenduduk(v)
    }

    override fun getItemCount(): Int {
        return dataPakaian.size
    }

    override fun onBindViewHolder(holder: AdapterDataPenduduk.HolderDataPenduduk, position: Int) {
        val data = dataPakaian.get(position)
        holder.txId.setText(data.get("id_pakaian"))
        holder.txNmPakaian.setText(data.get("nm_pakaian"))
        holder.txKategori.setText(data.get("nm_kategori"))
        holder.txHarga.setText("Rp. "+data.get("harga"))
        holder.txStok.setText(data.get("stok"))

        if (position.rem(2) == 0) holder.cLayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else holder.cLayout.setBackgroundColor(Color.rgb(255,255,245))

        holder.cLayout.setOnClickListener(View.OnClickListener {
            val pos = mainActivity.daftarKategori.indexOf(data.get("nm_kategori"))
            mainActivity.spinKategori.setSelection(pos)
            mainActivity.edId.setText(data.get("id_pakaian"))
            mainActivity.edNamaPenduduk.setText(data.get("nm_pakaian"))
            Picasso.get().load(data.get("url")).into(mainActivity.imUpload);
            mainActivity.edHarga.setText(data.get("harga"))
            mainActivity.edStok.setText(data.get("stok"))
        })

        if (!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(holder.photo);
    }

    class HolderDataPenduduk(v : View) : RecyclerView.ViewHolder(v){
        val txId = v.findViewById<TextView>(R.id.txId)
        val txNmPakaian = v.findViewById<TextView>(R.id.txNmPakaian)
        val txKategori = v.findViewById<TextView>(R.id.txKategori)
        val photo = v.findViewById<ImageView>(R.id.imageView)
        val txHarga = v.findViewById<TextView>(R.id.txHarga)
        val txStok = v.findViewById<TextView>(R.id.txStok)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
    }

}