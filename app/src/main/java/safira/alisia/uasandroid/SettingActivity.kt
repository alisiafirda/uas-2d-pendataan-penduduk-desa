package safira.alisia.uasandroid

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_setting.*

class SettingActivity : AppCompatActivity(), View.OnClickListener, SeekBar.OnSeekBarChangeListener {



    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    var BG_TITLE_COLOR = "bg_title_color"
    var FONT_TITLE_COLOR = "font_title_color"
    var FONT_TITLE_SIZE = "font_title__size"
    var TITLE_TEXT = "title_text"
    var DETAIL_FONT_SIZE = "detail_font_size"
    var DETAIL_TEXT = "detail_text"
    val DEF_TITLE_COLOR = "RED"
    val DEF_FONT_TITLE_COLOR  = "WHITE"
    val DEF_FONT_TITLE_SIZE = 24
    val DEF_TITLE_TEXT = "TITLE TEXT"
    val DEF_DETAIL_FONT_SIZE = 18
    val DEF_DETAIL_TEXT =
        "Tari Hapsari tidak menyangka pernikahannya jadi mimpi buruk. " +
                " Hari pertama tiba di rumah Byantara, suaminya, Tari langsung dihadapkan pada perjanjian pernikahan yang isinya " +
                " mengatakan bahwa mereka akan bercerai dalam waktu satu tahun. Bian berencana menikahi Sarah, kekasihnya. " +
                " Bian melakukan pernikahan hanya demi bakti kepada orangtua. Tari tidak menyerah, ia mencoba mengambil hati Bian. " +
                " Namun sekuat apa pun Tari mencoba, selalu ada Sarah di antara mereka. "
    var FontTitleColor: String = ""
    var BGTitleColor: String = ""
    val arrayBGTitleColor = arrayOf("BLUE", "YELLOW", "GREEN", "BLACK")
    lateinit var adapterSpin: ArrayAdapter<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        //listener
        btnSimpan.setOnClickListener(this)
        rgBGHColor.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rbBlue -> FontTitleColor = "BLUE"
                R.id.rbYellow -> FontTitleColor = "YELLOW"
                R.id.rbGreen -> FontTitleColor = "GREEN"
                R.id.rbBlack -> FontTitleColor = "BLACK"
            }
        }
        adapterSpin = ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayBGTitleColor)
        spBGTitleColor.adapter = adapterSpin
        spBGTitleColor.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                BGTitleColor = adapterSpin.getItem(position).toString()
            }
        }

        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        var spinnerselection =
            adapterSpin.getPosition(preferences.getString(BG_TITLE_COLOR, DEF_TITLE_COLOR))
        spBGTitleColor.setSelection(spinnerselection, true)
        var rgselection = preferences.getString(FONT_TITLE_COLOR, DEF_FONT_TITLE_COLOR)
        if (rgselection == "BLUE") {
            rgBGHColor.check(R.id.rbBlue)
        } else if (rgselection == "YELLOW") {
            rgBGHColor.check(R.id.rbYellow)
        } else if (rgselection == "GREEN") {
            rgBGHColor.check(R.id.rbGreen)
        } else {
            rgBGHColor.check(R.id.rbBlack)
        }
        sbFTitle.progress = preferences.getInt(FONT_TITLE_SIZE, DEF_FONT_TITLE_SIZE)
        sbFTitle.setOnSeekBarChangeListener(this)
        sbFDetail.progress = preferences.getInt(DETAIL_FONT_SIZE, DEF_DETAIL_FONT_SIZE)
        sbFDetail.setOnSeekBarChangeListener(this)
        edJudul.setText(preferences.getString(TITLE_TEXT, DEF_TITLE_TEXT))
        edDetail.setText(preferences.getString(DETAIL_TEXT, DEF_DETAIL_TEXT))
    }
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnSimpan -> {
                //save configuration to SharedPreferences
                preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
                val prefEditor = preferences.edit()
                prefEditor.putString(BG_TITLE_COLOR, BGTitleColor)
                prefEditor.putString(FONT_TITLE_COLOR, FontTitleColor)
                prefEditor.putInt(FONT_TITLE_SIZE, sbFTitle.progress)
                prefEditor.putInt(DETAIL_FONT_SIZE, sbFDetail.progress)
                prefEditor.putString(TITLE_TEXT, edJudul.text.toString())
                prefEditor.putString(DETAIL_TEXT, edDetail.text.toString())
                prefEditor.commit()
                Toast.makeText(this, "Setting Disimpan", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        when (seekBar?.id) {
            R.id.sbFTitle -> {

            }
            R.id.sbFDetail -> {

            }
        }
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {

    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {

    }



}
